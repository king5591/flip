import { Employee } from "./employee";
import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent {
  allEmployees: Employee[] = [
    {
      name: "James",
      dept: "Finance",
      designation: "Manager",
      location: "London",
      attendance: 22,
    },
    {
      name: "Rohit",
      dept: "Product",
      designation: "Manager",
      location: "Delhi",
      attendance: 24,
    },
    {
      name: "Bob",
      dept: "Operations",
      designation: "Manager",
      location: "New	York",
      attendance: 19,
    },
    {
      name: "Neha",
      dept: "Tech",
      designation: "Manager",
      location: "Bengaluru",
      attendance: 21,
    },
    {
      name: "Steve",
      dept: "Finance",
      designation: "Trainee",
      location: "London",
      attendance: 9,
    },
    {
      name: "Vivek",
      dept: "Product",
      designation: "Trainee",
      location: "Delhi",
      attendance: 25,
    },
    {
      name: "Julia",
      dept: "Operations",
      designation: "Trainee",
      location: "New	York",
      attendance: 7,
    },
    {
      name: "Manish",
      dept: "Tech",
      designation: "Trainee",
      location: "Bengaluru",
      attendance: 23,
    },
    {
      name: "William",
      dept: "Finance",
      designation: "Director",
      location: "London",
      attendance: 21,
    },
    {
      name: "Amit",
      dept: "Product",
      designation: "President",
      location: "Delhi",
      attendance: 20,
    },
    {
      name: "Mary",
      dept: "Operations",
      designation: "Director",
      location: "New	York",
      attendance: 25,
    },
    {
      name: "Matt",
      dept: "Tech",
      designation: "Team	Lead",
      location: "Bengaluru",
      attendance: 23,
    },
  ];
  showEmployees: Employee[] = this.allEmployees;
  department: string = "";
  sortby: string = "";
  deptName = ["All", "Finance", "Product", "Operations", "Tech"];
  sortOption = [
    "Name Ascending",
    "Name Descending",
    "Attendance High to Low",
    "Attendance Low to High",
  ];
}
