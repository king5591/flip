import { ActivatedRoute, Router } from "@angular/router";
import { Component, Input, OnInit } from "@angular/core";

@Component({
  selector: "deal",
  templateUrl: "./deal.component.html",
  styleUrls: ["./deal.component.css"],
})
export class DealComponent implements OnInit {
  @Input("mobile") mobile;
  constructor(private router: Router, private route: ActivatedRoute) {}

  ngOnInit() {
    // console.log(this.mobile);
  }
  prodDetail(id) {
    this.router.navigate(
      ["/home", this.mobile.category, this.mobile.brand, id],
      { relativeTo: this.route }
    );
  }
}
