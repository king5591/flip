import { ActivatedRoute, Router } from "@angular/router";
import { NetService } from "./../net.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-checkout",
  templateUrl: "./checkout.component.html",
  styleUrls: ["./checkout.component.css"],
})
export class CheckoutComponent implements OnInit {
  items: number = 0;
  totalPrice: number = 0;
  prod;

  constructor(
    private netService: NetService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.prod = this.netService.data;
    for (let i = 0; i < this.prod.length; i++) {
      this.items += this.prod[i].items;
      this.totalPrice += this.prod[i].prod.price * this.prod[i].items;
    }
    this.netService.items = this.items;
    // console.log(this.items);
    // console.log(this.prod);
  }
  priceDecs(p) {
    this.items--;
    this.totalPrice -= p;
    this.netService.items = this.items;
  }
  priceIncs(p) {
    this.items++;
    this.totalPrice += p;
    this.netService.items = this.items;
  }
}
