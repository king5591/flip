export class Employee {
  name: string;
  dept: string;
  designation: string;
  location: string;
  attendance: number;
}
