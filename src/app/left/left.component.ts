import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "left",
  templateUrl: "./left.component.html",
  styleUrls: ["./left.component.css"],
})
export class LeftComponent implements OnInit {
  @Input("ram") ram;
  @Input("rate") rating;
  @Input("price") price;
  @Input("assured") assured;
  isExpanded: boolean;
  priceExpand: boolean;
  ratingExpand: boolean;
  @Output() selOption = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  emitChange() {
    this.selOption.emit();
  }
  ramZippy() {
    this.isExpanded = !this.isExpanded;
  }
  priceZippy() {
    this.priceExpand = !this.priceExpand;
  }
  ratingZippy() {
    this.ratingExpand = !this.ratingExpand;
  }
}
