import { ActivatedRoute, Router } from "@angular/router";
import { NetService } from "./../net.service";
import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-cart",
  templateUrl: "./cart.component.html",
  styleUrls: ["./cart.component.css"],
})
export class CartComponent implements OnInit {
  @Input("prod") prod;
  @Input("items") items;
  @Input("totalPrice") totalPrice;
  @Output() incsPrice = new EventEmitter();
  @Output() decsPrice = new EventEmitter();

  constructor(
    private netService: NetService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {}
  incs() {
    this.items++;
    this.prod.items++;
    this.incsPrice.emit(this.prod.prod.price);
  }
  decs() {
    if (this.prod.items > 1) {
      this.items--;
      this.prod.items--;
      this.decsPrice.emit(this.prod.prod.price);
    }
  }
}
