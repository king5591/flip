import { CheckoutComponent } from "./checkout/checkout.component";
import { ProdDetailComponent } from "./prod-detail/prod-detail.component";
import { ProductsComponent } from "./products/products.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./home/home.component";

const routes: Routes = [
  { path: "", component: HomeComponent },
  { path: "home", component: HomeComponent },
  { path: "home/checkout", component: CheckoutComponent },
  { path: "home/:category", component: ProductsComponent },
  { path: "home/:category/:brand", component: ProductsComponent },
  { path: "home/:category/:brand/:id", component: ProdDetailComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
