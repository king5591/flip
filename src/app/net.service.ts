import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class NetService {
  data = [];
  items: number = 0;
  totalPrice: number = 0;
  constructor(private httpClient: HttpClient) {}

  getData(url) {
    return this.httpClient.get(url);
  }
  postData(url, obj) {
    return this.httpClient.post(url, obj);
  }
  deleteData(url) {
    return this.httpClient.delete(url);
  }
}
