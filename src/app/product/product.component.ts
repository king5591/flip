import { ActivatedRoute, Router } from "@angular/router";
import { Component, Input, OnInit } from "@angular/core";

@Component({
  selector: "product",
  templateUrl: "./product.component.html",
  styleUrls: ["./product.component.css"],
})
export class ProductComponent implements OnInit {
  @Input("prod") prod;
  heart: boolean;
  constructor(private router: Router, private route: ActivatedRoute) {}

  ngOnInit() {
    // console.log(this.mobile);
  }
  like() {
    this.heart = !this.heart;
  }
  prodDetails(id) {
    this.router.navigate(["/home", this.prod.category, this.prod.brand, id], {
      relativeTo: this.route,
    });
  }
}
