import { Employee } from "./employee";
import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "short",
})
export class ShortPipe implements PipeTransform {
  transform(value: string, len: number): string {
    return value.substring(0, len);
  }
  /*
  transform(employees: Employee[], department: string, sort: string): any {
    var sts = employees;
    if (department != "" && department != "All") {
      sts = employees.filter((item) => item.dept == department);
    }
    switch (sort) {
      case "Name Ascending":
        return sts.sort(this.sortNameAsc);
      case "Name Descending":
        return sts.sort(this.sortNameDsc);
      case "Attendance High to Low":
        return sts.sort(this.sortAttenHighLow);
      case "Attendance Low to High":
        return sts.sort(this.sortAttenLowHigh);
      default:
        return sts;
    }
    return null;
  }
  sortNameAsc(s1: Employee, s2: Employee) {
    return s1.name.localeCompare(s2.name);
  }
  sortNameDsc(s1: Employee, s2: Employee) {
    return s2.name.localeCompare(s1.name);
  }
  sortAttenHighLow(s1: Employee, s2: Employee) {
    return s2.attendance - s1.attendance;
  }
  sortAttenLowHigh(s1: Employee, s2: Employee) {
    return s1.attendance - s2.attendance;
  }*/
}
