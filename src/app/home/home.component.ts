import { Component, OnInit } from "@angular/core";
import { NetService } from "../net.service";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"],
})
export class HomeComponent implements OnInit {
  data;
  url = "https://us-central1-fytpo-f6ed3.cloudfunctions.net/app/deals";
  startIndex = 0;
  endIndex = 5;
  mobiles;
  items;

  constructor(private netService: NetService) {}

  ngOnInit() {
    this.items = this.netService.items;
    this.netService.getData(this.url).subscribe((resp) => {
      this.data = resp;
      this.mobiles = this.data.slice(this.startIndex, this.endIndex);
      // console.log(this.mobiles);
    });
  }
  previous() {
    if (this.startIndex > 0) {
      this.startIndex = this.startIndex - 5;
      this.endIndex = this.endIndex - 5;
    }
    this.mobiles = this.data.slice(this.startIndex, this.endIndex);
    // console.log(this.mobiles);
  }
  next() {
    if (this.endIndex < this.data.length) {
      this.startIndex = this.startIndex + 5;
      this.endIndex = this.endIndex + 5;
    }
    this.mobiles = this.data.slice(this.startIndex, this.endIndex);
    // console.log(this.mobiles);
  }
}
