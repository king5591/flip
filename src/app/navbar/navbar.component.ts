import { NetService } from "./../net.service";
import { Component, Input, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.css"],
})
export class NavbarComponent implements OnInit {
  q;
  page = 1;
  @Input("items") items;
  category;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private netService: NetService
  ) {}

  ngOnInit() {}
  selectMobiles(s) {
    this.category = "Mobiles";
    this.router.navigate(["/home", this.category, s], {
      relativeTo: this.route,
      queryParams: { page: 1 },
    });
  }
  selectLaptop(s) {
    this.category = "Laptops";
    this.router.navigate(["/home", this.category, s], {
      relativeTo: this.route,
      queryParams: { page: 1 },
    });
  }
  selectCameras(s) {
    this.category = "Cameras";
    this.router.navigate(["/home", this.category, s], {
      relativeTo: this.route,
      queryParams: { page: 1 },
    });
  }
  search() {
    this.router.navigate(["/home", "Mobiles"], {
      relativeTo: this.route,
      queryParams: { q: this.q, page: 1 },
    });
  }
}
