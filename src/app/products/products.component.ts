import { NetService } from "./../net.service";
import { ActivatedRoute, Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-products",
  templateUrl: "./products.component.html",
  styleUrls: ["./products.component.css"],
})
export class ProductsComponent implements OnInit {
  category;
  brand;
  url: string;
  data;
  products;
  pageNumber;
  numOfItems;
  numberOfPages;
  pageArr;
  pageInfo;
  totalItems;
  page;
  sort;
  pop: boolean;
  asc: boolean;
  desc: boolean;
  ramArray = [
    { display: "6 GB and More", value: ">6" },
    { display: "4 GB", value: "<4" },
    { display: "3 GB", value: "<3" },
    { display: "2 GB", value: "<2" },
  ];
  ramStruct;
  ram: string;
  priceArray = [
    { display: "0-5000", value: "0-5000" },
    { display: "5000-10000", value: "5000-10000" },
    { display: "10000-20000", value: "10000-20000" },
    { display: "More than 20000", value: "20000" },
  ];
  price: string;
  priceStruct;
  rate: string;
  rating = [
    { display: 4, value: ">4" },
    { display: 3, value: ">3" },
    { display: 2, value: ">2" },
    { display: 1, value: ">1" },
  ];
  ratingStruct;
  assured;
  assStruct;
  q;
  items;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private netService: NetService
  ) {}

  ngOnInit() {
    this.items = this.netService.items;
    this.brand = undefined;
    this.route.paramMap.subscribe((param) => {
      this.brand = undefined;
      this.category = param.get("category");
      this.brand = param.get("brand");
      if (this.category == "Mobiles") {
        this.url =
          "https://us-central1-fytpo-f6ed3.cloudfunctions.net/app/products";
      } else {
        this.url = "https://calm-atoll-02843.herokuapp.com/products";
      }
      this.makeStructure();
      if (this.category) {
        if (this.brand) {
          this.route.queryParamMap.subscribe((param) => {
            this.page = param.get("page");
            this.sort = param.get("sort");
            this.assured = param.get("assured");
            this.ram = param.get("ram");
            this.rate = param.get("rating");
            this.price = param.get("price");
            this.q = param.get("q");
            // //console.log(this.ram, this.assured, this.rate, this.price, this.q);
            // //console.log(this.page);
            // //console.log(this.sort);

            if (
              this.assured &&
              this.sort &&
              this.ram &&
              this.rate &&
              this.price &&
              this.q
            ) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&assured=" +
                    this.assured +
                    "&sort=" +
                    this.sort +
                    "&ram=" +
                    this.ram +
                    "&rating=" +
                    this.rate +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  // //console.log(this.data);
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (
              this.assured &&
              this.sort &&
              this.ram &&
              this.rate &&
              this.price
            ) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&assured=" +
                    this.assured +
                    "&sort=" +
                    this.sort +
                    "&ram=" +
                    this.ram +
                    "&rating=" +
                    this.rate +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (
              this.assured &&
              this.sort &&
              this.ram &&
              this.rate &&
              this.q
            ) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&assured=" +
                    this.assured +
                    "&sort=" +
                    this.sort +
                    "&ram=" +
                    this.ram +
                    "&rating=" +
                    this.rate
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.sort && this.ram && this.rate) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&assured=" +
                    this.assured +
                    "&sort=" +
                    this.sort +
                    "&ram=" +
                    this.ram +
                    "&rating=" +
                    this.rate
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (
              this.assured &&
              this.sort &&
              this.ram &&
              this.price &&
              this.q
            ) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&assured=" +
                    this.assured +
                    "&sort=" +
                    this.sort +
                    "&ram=" +
                    this.ram +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.sort && this.ram && this.price) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&assured=" +
                    this.assured +
                    "&sort=" +
                    this.sort +
                    "&ram=" +
                    this.ram +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (
              this.assured &&
              this.ram &&
              this.rate &&
              this.price &&
              this.q
            ) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&assured=" +
                    this.assured +
                    "&ram=" +
                    this.ram +
                    "&rating=" +
                    this.rate +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.ram && this.rate && this.price) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&assured=" +
                    this.assured +
                    "&ram=" +
                    this.ram +
                    "&rating=" +
                    this.rate +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (
              this.assured &&
              this.sort &&
              this.rate &&
              this.price &&
              this.q
            ) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&assured=" +
                    this.assured +
                    "&sort=" +
                    this.sort +
                    "&rating=" +
                    this.rate +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.sort && this.rate && this.price) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&assured=" +
                    this.assured +
                    "&sort=" +
                    this.sort +
                    "&rating=" +
                    this.rate +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (
              this.ram &&
              this.rate &&
              this.price &&
              this.sort &&
              this.q
            ) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&sort=" +
                    this.sort +
                    "&ram=" +
                    this.ram +
                    "&rating=" +
                    this.rate +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.ram && this.rate && this.price && this.sort) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&sort=" +
                    this.sort +
                    "&ram=" +
                    this.ram +
                    "&rating=" +
                    this.rate +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.sort && this.ram && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&assured=" +
                    this.assured +
                    "&sort=" +
                    this.sort +
                    "&ram=" +
                    this.ram
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.sort && this.ram) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&assured=" +
                    this.assured +
                    "&sort=" +
                    this.sort +
                    "&ram=" +
                    this.ram
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.sort && this.rate && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&assured=" +
                    this.assured +
                    "&sort=" +
                    this.sort +
                    "&rating=" +
                    this.rate
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.sort && this.rate) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&assured=" +
                    this.assured +
                    "&sort=" +
                    this.sort +
                    "&rating=" +
                    this.rate
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.sort && this.price && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&assured=" +
                    this.assured +
                    "&sort=" +
                    this.sort +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.sort && this.price) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&assured=" +
                    this.assured +
                    "&sort=" +
                    this.sort +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.ram && this.rate && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&assured=" +
                    this.assured +
                    "&ram=" +
                    this.ram +
                    "&rating=" +
                    this.rate
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.ram && this.rate) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&assured=" +
                    this.assured +
                    "&ram=" +
                    this.ram +
                    "&rating=" +
                    this.rate
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.ram && this.price && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&assured=" +
                    this.assured +
                    "&ram=" +
                    this.ram +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.ram && this.price) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&assured=" +
                    this.assured +
                    "&ram=" +
                    this.ram +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.sort && this.ram && this.rate && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&sort=" +
                    this.sort +
                    "&ram=" +
                    this.ram +
                    "&rating=" +
                    this.rate
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.sort && this.ram && this.rate) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&sort=" +
                    this.sort +
                    "&ram=" +
                    this.ram +
                    "&rating=" +
                    this.rate
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.sort && this.ram && this.price && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&sort=" +
                    this.sort +
                    "&ram=" +
                    this.ram +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.sort && this.ram && this.price) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&sort=" +
                    this.sort +
                    "&ram=" +
                    this.ram +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.sort && this.rate && this.price && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&sort=" +
                    this.sort +
                    "&rating=" +
                    this.rate +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.sort && this.rate && this.price) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&sort=" +
                    this.sort +
                    "&rating=" +
                    this.rate +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.ram && this.rate && this.price && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&ram=" +
                    this.ram +
                    "&rating=" +
                    this.rate +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.ram && this.rate && this.price) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&ram=" +
                    this.ram +
                    "&rating=" +
                    this.rate +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.sort && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&assured=" +
                    this.assured +
                    "&sort=" +
                    this.sort
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.sort) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&assured=" +
                    this.assured +
                    "&sort=" +
                    this.sort
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.ram && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&assured=" +
                    this.assured +
                    "&ram=" +
                    this.ram
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.ram) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&assured=" +
                    this.assured +
                    "&ram=" +
                    this.ram
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.rate && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&assured=" +
                    this.assured +
                    "&rating=" +
                    this.rate
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.rate) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&assured=" +
                    this.assured +
                    "&rating=" +
                    this.rate
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.price && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&assured=" +
                    this.assured +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.price) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&assured=" +
                    this.assured +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.sort && this.ram && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&sort=" +
                    this.sort +
                    "&ram=" +
                    this.ram
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.sort && this.ram) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&sort=" +
                    this.sort +
                    "&ram=" +
                    this.ram
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.sort && this.rate && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&sort=" +
                    this.sort +
                    "&rating=" +
                    this.rate
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.sort && this.rate) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&sort=" +
                    this.sort +
                    "&rating=" +
                    this.rate
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.sort && this.price && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&sort=" +
                    this.sort +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.sort && this.price) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&sort=" +
                    this.sort +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.ram && this.rate && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&ram=" +
                    this.ram +
                    "&rating=" +
                    this.rate
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.ram && this.rate) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&ram=" +
                    this.ram +
                    "&rating=" +
                    this.rate
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.ram && this.price && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&ram=" +
                    this.ram +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.ram && this.price) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&ram=" +
                    this.ram +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.rate && this.price && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&rating=" +
                    this.rate +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.rate && this.price) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&rating=" +
                    this.rate +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&assured=" +
                    this.assured
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.ram && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&ram=" +
                    this.ram
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.rate && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&rating=" +
                    this.rate
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.price && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.sort && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&sort=" +
                    this.sort
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&assured=" +
                    this.assured
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.ram) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&ram=" +
                    this.ram
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.rate) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&rating=" +
                    this.rate
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.price) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.sort) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&sort=" +
                    this.sort
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    this.brand +
                    "?page=" +
                    this.page
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            }
          });
        } else {
          this.route.queryParamMap.subscribe((param) => {
            this.page = param.get("page");
            this.sort = param.get("sort");
            this.assured = param.get("assured");
            this.ram = param.get("ram");
            this.rate = param.get("rating");
            this.price = param.get("price");
            this.q = param.get("q");

            if (
              this.assured &&
              this.sort &&
              this.ram &&
              this.rate &&
              this.price &&
              this.q
            ) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&assured=" +
                    this.assured +
                    "&sort=" +
                    this.sort +
                    "&ram=" +
                    this.ram +
                    "&rating=" +
                    this.rate +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (
              this.assured &&
              this.sort &&
              this.ram &&
              this.rate &&
              this.price
            ) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&assured=" +
                    this.assured +
                    "&sort=" +
                    this.sort +
                    "&ram=" +
                    this.ram +
                    "&rating=" +
                    this.rate +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (
              this.assured &&
              this.sort &&
              this.ram &&
              this.rate &&
              this.q
            ) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&assured=" +
                    this.assured +
                    "&sort=" +
                    this.sort +
                    "&ram=" +
                    this.ram +
                    "&rating=" +
                    this.rate
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.sort && this.ram && this.rate) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&assured=" +
                    this.assured +
                    "&sort=" +
                    this.sort +
                    "&ram=" +
                    this.ram +
                    "&rating=" +
                    this.rate
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (
              this.assured &&
              this.sort &&
              this.ram &&
              this.price &&
              this.q
            ) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&assured=" +
                    this.assured +
                    "&sort=" +
                    this.sort +
                    "&ram=" +
                    this.ram +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.sort && this.ram && this.price) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&assured=" +
                    this.assured +
                    "&sort=" +
                    this.sort +
                    "&ram=" +
                    this.ram +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (
              this.assured &&
              this.ram &&
              this.rate &&
              this.price &&
              this.q
            ) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&assured=" +
                    this.assured +
                    "&ram=" +
                    this.ram +
                    "&rating=" +
                    this.rate +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.ram && this.rate && this.price) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&assured=" +
                    this.assured +
                    "&ram=" +
                    this.ram +
                    "&rating=" +
                    this.rate +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (
              this.assured &&
              this.sort &&
              this.rate &&
              this.price &&
              this.q
            ) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&assured=" +
                    this.assured +
                    "&sort=" +
                    this.sort +
                    "&rating=" +
                    this.rate +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.sort && this.rate && this.price) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&assured=" +
                    this.assured +
                    "&sort=" +
                    this.sort +
                    "&rating=" +
                    this.rate +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (
              this.ram &&
              this.rate &&
              this.price &&
              this.sort &&
              this.q
            ) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&sort=" +
                    this.sort +
                    "&ram=" +
                    this.ram +
                    "&rating=" +
                    this.rate +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.ram && this.rate && this.price && this.sort) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&sort=" +
                    this.sort +
                    "&ram=" +
                    this.ram +
                    "&rating=" +
                    this.rate +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.sort && this.ram && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&assured=" +
                    this.assured +
                    "&sort=" +
                    this.sort +
                    "&ram=" +
                    this.ram
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.sort && this.ram) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&assured=" +
                    this.assured +
                    "&sort=" +
                    this.sort +
                    "&ram=" +
                    this.ram
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.sort && this.rate && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&assured=" +
                    this.assured +
                    "&sort=" +
                    this.sort +
                    "&rating=" +
                    this.rate
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.sort && this.rate) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&assured=" +
                    this.assured +
                    "&sort=" +
                    this.sort +
                    "&rating=" +
                    this.rate
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.sort && this.price && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&assured=" +
                    this.assured +
                    "&sort=" +
                    this.sort +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.sort && this.price) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&assured=" +
                    this.assured +
                    "&sort=" +
                    this.sort +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.ram && this.rate && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&assured=" +
                    this.assured +
                    "&ram=" +
                    this.ram +
                    "&rating=" +
                    this.rate
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.ram && this.rate) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&assured=" +
                    this.assured +
                    "&ram=" +
                    this.ram +
                    "&rating=" +
                    this.rate
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.ram && this.price && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&assured=" +
                    this.assured +
                    "&ram=" +
                    this.ram +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.ram && this.price) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&assured=" +
                    this.assured +
                    "&ram=" +
                    this.ram +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.sort && this.ram && this.rate && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&sort=" +
                    this.sort +
                    "&ram=" +
                    this.ram +
                    "&rating=" +
                    this.rate
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.sort && this.ram && this.rate) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&sort=" +
                    this.sort +
                    "&ram=" +
                    this.ram +
                    "&rating=" +
                    this.rate
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.sort && this.ram && this.price && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&sort=" +
                    this.sort +
                    "&ram=" +
                    this.ram +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.sort && this.ram && this.price) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&sort=" +
                    this.sort +
                    "&ram=" +
                    this.ram +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.sort && this.rate && this.price && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&sort=" +
                    this.sort +
                    "&rating=" +
                    this.rate +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.sort && this.rate && this.price) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&sort=" +
                    this.sort +
                    "&rating=" +
                    this.rate +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.ram && this.rate && this.price && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&ram=" +
                    this.ram +
                    "&rating=" +
                    this.rate +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.ram && this.rate && this.price) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&ram=" +
                    this.ram +
                    "&rating=" +
                    this.rate +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.sort && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&assured=" +
                    this.assured +
                    "&sort=" +
                    this.sort
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.sort) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&assured=" +
                    this.assured +
                    "&sort=" +
                    this.sort
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.ram && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&assured=" +
                    this.assured +
                    "&ram=" +
                    this.ram
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.ram) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&assured=" +
                    this.assured +
                    "&ram=" +
                    this.ram
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.rate && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&assured=" +
                    this.assured +
                    "&rating=" +
                    this.rate
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.rate) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&assured=" +
                    this.assured +
                    "&rating=" +
                    this.rate
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.price && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&assured=" +
                    this.assured +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.price) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&assured=" +
                    this.assured +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.sort && this.ram && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&sort=" +
                    this.sort +
                    "&ram=" +
                    this.ram
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.sort && this.ram) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&sort=" +
                    this.sort +
                    "&ram=" +
                    this.ram
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.sort && this.rate && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&sort=" +
                    this.sort +
                    "&rating=" +
                    this.rate
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.sort && this.rate) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&sort=" +
                    this.sort +
                    "&rating=" +
                    this.rate
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.sort && this.price && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&sort=" +
                    this.sort +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.sort && this.price) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&sort=" +
                    this.sort +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.ram && this.rate && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&ram=" +
                    this.ram +
                    "&rating=" +
                    this.rate
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.ram && this.rate) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&ram=" +
                    this.ram +
                    "&rating=" +
                    this.rate
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.ram && this.price && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&ram=" +
                    this.ram +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.ram && this.price) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&ram=" +
                    this.ram +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.rate && this.price && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&rating=" +
                    this.rate +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.rate && this.price) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&rating=" +
                    this.rate +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&assured=" +
                    this.assured
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.ram && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&ram=" +
                    this.ram
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.rate && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&rating=" +
                    this.rate
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.price && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.sort && this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q +
                    "&sort=" +
                    this.sort
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.assured) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&assured=" +
                    this.assured
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.ram) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&ram=" +
                    this.ram
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.rate) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&rating=" +
                    this.rate
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.price) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&price=" +
                    this.price
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.sort) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "/" +
                    "?page=" +
                    this.page +
                    "&sort=" +
                    this.sort
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else if (this.q) {
              this.netService
                .getData(
                  this.url +
                    "/" +
                    this.category +
                    "?page=" +
                    this.page +
                    "&q=" +
                    this.q
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            } else {
              this.netService
                .getData(
                  this.url + "/" + this.category + "/" + "?page=" + this.page
                )
                .subscribe((resp) => {
                  this.data = resp;
                  this.products = this.data.data;
                  this.pageInfo = this.data.pageInfo;
                  this.pageNumber = this.pageInfo.pageNumber;
                  this.numberOfPages = this.pageInfo.numberOfPages;
                  this.numOfItems = this.pageInfo.numOfItems;
                  this.totalItems = this.pageInfo.totalItemCount;
                  let temp = [];
                  for (let i = 1; i <= this.numberOfPages; i++) {
                    temp.push(i);
                  }
                  this.pageArr = temp;
                  console.log(this.products);
                });
            }
          });
        }
      }
    });
  }
  makeStructure() {
    this.ram = this.ram ? this.ram : "";
    this.rate = this.rate ? this.rate : "";
    this.price = this.price ? this.price : "";
    this.assured = this.assured ? this.assured : false;
    //console.log(this.assured);
    this.assStruct = { assured: "assured", selected: false };
    this.ramStruct = this.ramArray.map((r1) => ({
      display: r1.display,
      ram: r1.value,
      selected: false,
    }));
    this.priceStruct = this.priceArray.map((p1) => ({
      display: p1.display,
      price: p1.value,
      selected: false,
    }));
    this.ratingStruct = this.rating.map((r1) => ({
      display: r1.display,
      rating: r1.value,
      selected: false,
    }));
    let temp1 = this.ram.split(",");
    let temp2 = this.price.split(",");
    let temp3 = this.rate.split(",");
    if (this.assured === this.assStruct.assured) {
      this.assStruct.selected = true;
    }
    for (let i = 0; i < temp1.length; i++) {
      let item = this.ramStruct.find((r1) => r1.ram == temp1[i]);
      if (item) {
        item.selected = true;
      }
    }
    for (let i = 0; i < temp2.length; i++) {
      let item = this.priceStruct.find((p1) => p1.price == temp2[i]);
      if (item) {
        item.selected = true;
      }
    }
    for (let i = 0; i < temp3.length; i++) {
      let item = this.ratingStruct.find((r1) => r1.rating == temp3[i]);
      if (item) {
        item.selected = true;
      }
    }
  }
  optChange() {
    this.pageNumber = 1;
    let qparam = {};
    if (this.pageNumber) {
      qparam["page"] = this.pageNumber;
    }
    if (this.assStruct.selected) {
      this.assured = this.assStruct.selected;
      //console.log(this.assured);
      qparam["assured"] = this.assured;
    }
    let ram1 = this.ramStruct.filter((r1) => r1.selected);
    let temp1 = ram1.map((r1) => r1.ram);
    this.ram = temp1.join(",");
    if (this.ram) {
      qparam["ram"] = this.ram;
    }
    let rate1 = this.ratingStruct.filter((r1) => r1.selected);
    let temp2 = rate1.map((r1) => r1.rating);
    this.rate = temp2.join(",");
    if (this.rate) {
      qparam["rating"] = this.rate;
    }
    let price1 = this.priceStruct.filter((p1) => p1.selected);
    let temp3 = price1.map((p1) => p1.price);
    this.price = temp3.join(",");
    if (this.price) {
      qparam["price"] = this.price;
    }
    if (this.sort) {
      qparam["sort"] = this.sort;
    }
    // //console.log(qparam);
    if (this.brand) {
      this.router.navigate(["/home", this.category, this.brand], {
        relativeTo: this.route,
        queryParams: qparam,
      });
    } else {
      this.router.navigate(["/home", this.category], {
        relativeTo: this.route,
        queryParams: qparam,
      });
    }
  }
  nextPage() {
    if (this.pageNumber < 4) {
      this.pageNumber = this.pageNumber + 1;
    }
    //console.log(this.pageNumber);
    let qparam = {};
    qparam["page"] = this.pageNumber;
    if (this.assured) {
      qparam["assured"] = this.assured;
    }
    if (this.ram) {
      qparam["ram"] = this.ram;
    }
    if (this.rate) {
      qparam["rating"] = this.rate;
    }
    if (this.price) {
      qparam["price"] = this.price;
    }
    if (this.sort) {
      qparam["sort"] = this.sort;
    }
    if (this.brand) {
      this.router.navigate(["/home", this.category, this.brand], {
        relativeTo: this.route,
        queryParams: qparam,
      });
    } else {
      this.router.navigate(["/home", this.category], {
        relativeTo: this.route,
        queryParams: qparam,
      });
    }
  }
  atPage(n) {
    this.pageNumber = n;
    let qparam = {};
    qparam["page"] = this.pageNumber;
    if (this.assured) {
      qparam["assured"] = this.assured;
    }
    if (this.ram) {
      qparam["ram"] = this.ram;
    }
    if (this.rate) {
      qparam["rating"] = this.rate;
    }
    if (this.price) {
      qparam["price"] = this.price;
    }
    if (this.sort) {
      qparam["sort"] = this.sort;
    }
    if (this.brand) {
      this.router.navigate(["/home", this.category, this.brand], {
        relativeTo: this.route,
        queryParams: qparam,
      });
    } else {
      this.router.navigate(["/home", this.category], {
        relativeTo: this.route,
        queryParams: qparam,
      });
    }
  }
  sortBy(s) {
    let qparam = {};
    qparam["page"] = 1;
    if (s == "Popularity") {
      this.pop = !this.pop;
      this.desc = false;
      this.asc = false;
      qparam["sort"] = "popularity";
    }
    if (s == "high to low") {
      this.desc = !this.desc;
      this.pop = false;
      this.asc = false;
      qparam["sort"] = "desc";
    }
    if (s == "low to high") {
      this.asc = !this.asc;
      this.desc = false;
      this.pop = false;
      qparam["sort"] = "asc";
    }
    if (this.assured) {
      qparam["assured"] = this.assured;
    }
    if (this.ram) {
      qparam["ram"] = this.ram;
    }
    if (this.rate) {
      qparam["rating"] = this.rate;
    }
    if (this.price) {
      qparam["price"] = this.price;
    }
    // //console.log(qparam);
    if (this.brand) {
      this.router.navigate(["/home", this.category, this.brand], {
        relativeTo: this.route,
        queryParams: qparam,
      });
    } else {
      this.router.navigate(["/home", this.category], {
        relativeTo: this.route,
        queryParams: qparam,
      });
    }
  }

  goto() {
    this.router.navigate(["/home", this.category], {
      queryParams: { page: 1 },
    });
  }
}
