import { ActivatedRoute, Router } from "@angular/router";
import { NetService } from "./../net.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-prod-detail",
  templateUrl: "./prod-detail.component.html",
  styleUrls: ["./prod-detail.component.css"],
})
export class ProdDetailComponent implements OnInit {
  data;
  url = "https://us-central1-fytpo-f6ed3.cloudfunctions.net/app/";
  bankUrl = "https://us-central1-fytpo-f6ed3.cloudfunctions.net/app/";
  id;
  pics;
  prod;
  imgList;
  bankOffers;
  items: number = 0;
  category;
  prodImg;

  constructor(
    private netService: NetService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.items = this.netService.items;
    this.route.paramMap.subscribe((param) => {
      this.category = param.get("category");
      this.id = param.get("id");
      if (this.category == "Mobiles") {
        this.url = "https://us-central1-fytpo-f6ed3.cloudfunctions.net/app/";
      } else {
        this.url = "https://calm-atoll-02843.herokuapp.com/";
      }
    });
    this.netService.getData(this.bankUrl + "bankOffers").subscribe((resp) => {
      this.bankOffers = resp;
      console.log(this.bankOffers);
    });
    this.netService
      .getData(this.url + "product/" + this.id)
      .subscribe((resp) => {
        this.data = resp;
        this.pics = this.data.pics;
        this.imgList = this.pics.imgList;
        this.prod = this.data.prod;
        this.prodImg = this.prod.img;
        // console.log(this.pics);
        // console.log(this.prod);
      });
  }
  prodImgs(i) {
    this.prodImg = this.imgList[i];
  }
  addToCart(p1) {
    if (this.netService.data) {
      let temp = this.netService.data.find((d1) => d1.prod.id == p1.id);
      if (!temp) {
        let item = { prod: p1, items: 1 };
        this.netService.data.push(item);
        this.items++;
      } else {
        temp.items++;
        this.items++;
      }
    } else {
      let item = { prod: p1, items: 1 };
      this.netService.data.push(item);
      this.items++;
    }
    console.log(this.items);
    this.router.navigate(
      ["/home", this.prod.category, this.prod.brand, this.prod.id],
      { relativeTo: this.route }
    );
    this.netService.items = this.items;
  }
  buyNow(p1) {
    if (this.netService.data.length) {
      let temp = this.netService.data.find((d1) => d1.prod.id == p1.id);
      console.log(temp);
      if (!temp) {
        let item = { prod: p1, items: 1 };
        this.netService.data.push(item);
        this.items++;
      } else {
        temp.items++;
        this.items++;
      }
    } else {
      let item = { prod: p1, items: 1 };
      this.netService.data.push(item);
      this.items++;
    }
    console.log(this.items);
    this.netService.items = this.items;
    this.router.navigate(["/home", "checkout"], { relativeTo: this.route });
  }
}
