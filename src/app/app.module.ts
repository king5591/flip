import { NetService } from "./net.service";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule } from "@angular/forms";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { ShortPipe } from "./short.pipe";
import { ExtraComponent } from "./extra/extra.component";
import { HomeComponent } from "./home/home.component";
import { NavbarComponent } from "./navbar/navbar.component";
import { DealComponent } from "./deal/deal.component";
import { ProductsComponent } from "./products/products.component";
import { ProdDetailComponent } from "./prod-detail/prod-detail.component";
import { LeftComponent } from "./left/left.component";
import { ProductComponent } from "./product/product.component";
import { CheckoutComponent } from './checkout/checkout.component';
import { CartComponent } from './cart/cart.component';

@NgModule({
  declarations: [		
    AppComponent,
    ShortPipe,
    ExtraComponent,
    HomeComponent,
    NavbarComponent,
    DealComponent,
    ProductsComponent,
    ProdDetailComponent,
    LeftComponent,
    ProductComponent,
      CheckoutComponent,
      CartComponent
   ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
  ],
  providers: [NetService],
  bootstrap: [AppComponent],
})
export class AppModule {}
